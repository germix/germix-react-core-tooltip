# Germix React Core - Tooltip

## About

Germix react core tooltip component

## Installation

```bash
npm install @germix/germix-react-core-tooltip
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
