import React, { PropsWithChildren } from 'react';

interface Props extends PropsWithChildren<any>
{
    direction?: 'top'|'left'|'right'|'bottom',
}


/**
 * @author Germán Martínez
 */
class Tooltip extends React.Component<Props>
{
    render()
    {
        const dir = this.props.direction || 'bottom';
        return (
<div
    className={"tooltip" + ' tooltip-dir-'+dir}
>
    <div className="tooltip-arrow"></div>
    <div className="tooltip-content">
        {this.props.children}
    </div>
</div>
        );
    }
}
export default Tooltip;
