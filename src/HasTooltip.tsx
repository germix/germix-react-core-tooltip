import React from 'react';

interface Props
{
    alwaysVisible?: boolean,
}
interface State
{
    hover: boolean,
}

/**
 * @author Germán Martínez
 */
class HasTooltip extends React.Component<Props,State>
{
    state =
    {
        hover: false,
    }
    render()
    {
        return (
<div className={"has-tooltip" + ((this.state.hover || this.props.alwaysVisible) ? ' on' : '')}
    onMouseOver={this.onMouseOver}
    onMouseLeave={this.onMouseLeave}
>
    { this.props.children }
</div>
        );
    }

    onMouseOver = (event) =>
    {
        if(!this.state.hover)
        {
            this.setState({
                hover: true
            })
        }
    }

    onMouseLeave = (event) =>
    {
        this.setState({
            hover: false
        })
    }
}
export default HasTooltip;
